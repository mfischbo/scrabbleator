package net.fischboeck.scrabbleator.web;

import net.fischboeck.scrabbleator.dto.GameDto;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author M.Fischböck
 */
@RestController
@RequestMapping("/1/games")
public class GameController {


    @PostMapping("")
    public GameDto createGame() {
        return new GameDto();
    }

}
