package net.fischboeck.scrabbleator.dto;

import lombok.Data;

/**
 * @author M.Fischböck
 */
@Data
public class PlayerDto {

    private String name;

    /**
     * States if the player is represented by the AI
     */
    private boolean humanPlayer;
}
