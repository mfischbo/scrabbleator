package net.fischboeck.scrabbleator.service;

import net.fischboeck.scrabbleator.model.GameContext;
import org.springframework.stereotype.Service;

/**
 * @author M.Fischböck
 */
@Service
public class GameService {


    public GameContext createGame() {
        return new GameContext();
    }
}
