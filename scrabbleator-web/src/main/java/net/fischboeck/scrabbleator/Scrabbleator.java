package net.fischboeck.scrabbleator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author M.Fischböck
 */
//@Configuration
//@EnableWebMvc
@SpringBootApplication
public class Scrabbleator { //extends WebMvcConfigurerAdapter {

    public static void main(String[] args) {
        SpringApplication.run(Scrabbleator.class, args);
    }

/*
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry
                .addResourceHandler("/webjars/**")
                .addResourceLocations("/webjars/");

        registry
                .addResourceHandler("/**")
                .addResourceLocations("/static/");
    }
    */
}
