package net.fischboeck.scrabbleator.util;

import org.apache.commons.io.IOUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Set;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * @author M.Fischböck
 */
public class AspellDictionaryReader {

    private static final Pattern pattern = Pattern.compile("[a-zA-Z]*");

    public static Set<String> readDictionary(File file) throws IOException {
        if (file == null)
            throw new IllegalArgumentException("The dictionary file must not be null");

        if (!file.exists() || !file.canRead())
            throw new IllegalArgumentException("Unable to read dictionary file");

        return readDictionary(new FileInputStream(file));
    }



    public static Set<String> readDictionary(InputStream input) throws IOException {

        String content = IOUtils.toString(input);
        String[] words = content.split("\\s");


        // filter all words that do not match /[a-zA-Z]/
        // filter all words that are longer than 14 characters
        // uppercase all remaining words
        Set<String> retval = Arrays.stream(words)
                .filter(word -> pattern.matcher(word).matches())
                .filter(word -> word.length() < 14)
                .filter(word -> word.length() > 3)
                .map(String::toUpperCase)
                .collect(Collectors.toSet());
        return retval;
    }
}
