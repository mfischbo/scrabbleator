package net.fischboeck.scrabbleator.engine;

import net.fischboeck.scrabbleator.model.ScrabbleBoard;
import net.fischboeck.scrabbleator.model.WordLayout;
import net.fischboeck.scrabbleator.util.AspellDictionaryReader;

import java.util.Set;

/**
 * @author M.Fischböck
 */
public class WordDictionaryValidator {

    private static Set<String> dictionary;

    static {
        try {
            dictionary = AspellDictionaryReader.readDictionary(
                    WordDictionaryValidator.class.getResourceAsStream("/dictionary.de"));
        } catch (Exception ex) {
            System.err.println("Failed to read dictionary during initalization.");
        }
    }


    public static boolean isValidWord(ScrabbleBoard board, WordLayout layout) {
        return (dictionary.contains(layout.asWord()));
    }
}
