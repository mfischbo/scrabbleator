package net.fischboeck.scrabbleator.engine;

import net.fischboeck.scrabbleator.model.ScrabbleBoard;
import net.fischboeck.scrabbleator.model.StoneBank;
import net.fischboeck.scrabbleator.model.WordLayout;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * The solver class is the entry point for the AI.
 * It finds solutions to layout a new word on the board given a {@link WordProducer} which holds the dictionary.
 * The solver utilizes a {@link Partitioner} to find possible {@link Window}s where words can be laid out.
 * Given the current {@link ScrabbleBoard} and the players {@link StoneBank} it returns a list of possible
 * {@link Solution}s where new words can be laid out on the board.
 *
 * @author M.Fischböck
 */
public class Solver {

    private WordProducer wordProducer;

    public Solver(Set<String> dictionary) {
        this.wordProducer = new WordProducer(dictionary);
    }


    public List<Solution> getSolutions(ScrabbleBoard board, StoneBank bank) {

        List<Solution> solutions = new ArrayList<>();

        for (int i=0; i < 15; i++) {

            Partitioner partitioner = new Partitioner(board, i);
            List<Window> windows = partitioner.findPartitions(WordLayout.Direction.Horizontal);
            windows.addAll(partitioner.findPartitions(WordLayout.Direction.Vertical));

            for (Window w : windows) {

                Set<WordLayout> layouts = wordProducer.findWordsMatchingPattern(board, bank, w);
                if (layouts.size() > 0)
                    solutions.add(new Solution(w, layouts));
            }
        }

        return solutions;
    }
}
