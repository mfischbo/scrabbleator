package net.fischboeck.scrabbleator.engine;

import net.fischboeck.scrabbleator.model.BoardField;
import net.fischboeck.scrabbleator.model.ScrabbleBoard;
import net.fischboeck.scrabbleator.model.ScrabbleBoard.Position;
import net.fischboeck.scrabbleator.model.Stone;
import net.fischboeck.scrabbleator.model.WordLayout;

import static net.fischboeck.scrabbleator.model.WordLayout.Direction.*;

/**
 * Validator that ensures a {@link WordLayout} is not in conflict with the {@link ScrabbleBoard} from a model perspective.
 * This class does not model the rules of the game, but ensures that the technical model itself is in a valid state.
 *
 * @author M.Fischböck
 */
public class WordLayoutValidator {

    /**
     * Returns whether or not the specified word layout is valid on the current game board.
     * This does not ensure that the layout is a valid word that is in the dictionary.
     *
     * @param board The current game board
     * @param layout The layout of stones to be made
     * @return True if the move is valid, false otherwise.
     */
    public static boolean isValid(ScrabbleBoard board, WordLayout layout) {

        // check if the word fits on the board
        if (exceedsBoardLimits(board, layout))
            return false;

        // ensure intersections are correct
        if (!intersectionsAreCorrect(board, layout))
            return false;

        // ensure initial layout condition is matched
        if (!initialLayoutConditionMatched(board, layout))
            return false;

        return true;
    }


    /**
     * Ensures that the word to be laid out 'fits' on the board and does not exceed the boards boundaries.
     *
     * @param board The board model to check against
     * @param layout The layout that should be laid
     * @return True if the model is not violated by the layout, false otherwise.
     */
    static boolean exceedsBoardLimits(ScrabbleBoard board, WordLayout layout) {

        // find the x / y position of the starting stone on the board
        Position position = board.getPositionOf(layout.getStartingField());

        if (layout.getLayoutDirection() == Horizontal) {
            if (position.x + layout.getStones().size() > 15)
                return true;
        }

        if (layout.getLayoutDirection() == Vertical) {
            if (position.y + layout.getStones().size() > 15)
                return true;
        }
        return false;
    }


    /**
     * Ensures that the provided word is congruent with the stones on the board that are already laid out.
     *
     * @param board The scrum board
     * @param layout The layout to be laid
     * @return True if the model is not violated by this move, false otherwise.
     */
    static boolean intersectionsAreCorrect(ScrabbleBoard board, WordLayout layout) {

        BoardField currentField = layout.getStartingField();
        for (int i=0; i < layout.getStones().size(); i++) {

            Stone currentStone = layout.getStones().get(i);

            // if no stone is on the 'currentField' we advance
            if (currentField.getStone() == null) {

                currentField = board.nextNeighbor(currentField, layout.getLayoutDirection());
                if (currentField == null)
                    return false; // in that case we exceeded the boards boundaries
                continue;
            }

            // if there is a stone on the 'currentField' we compare it to the word that should be laid for equality
            if (currentField.getStone() != null) {

                // the current stone in the word is different from the stone on the field...
                if (!currentField.getStone().equals(currentStone)) {

                    // if it's not a wildcard, this is a violation
                    if (!currentField.getStone().isWildcard()) {
                        return false;
                    }
                }

                currentField = board.nextNeighbor(currentField, layout.getLayoutDirection());
                if (currentField == null)
                    return false; // exceeded board boundary
            }
        }
        return true;
    }


    static boolean initialLayoutConditionMatched(ScrabbleBoard board, WordLayout layout) {

        if (board.isEmpty()) {

            int wordLength = layout.getStones().size();
            Position fieldPosition = board.getPositionOf(layout.getStartingField());

            if (layout.getLayoutDirection() == Horizontal) {
                return (fieldPosition.x <= 7) && (fieldPosition.x + layout.getStones().size() > 7);
            }

            if (layout.getLayoutDirection() == Vertical) {
                return (fieldPosition.y <= 7) && (fieldPosition.y + layout.getStones().size() > 7);
            }
        }
        return true;
    }
}
