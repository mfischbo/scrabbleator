package net.fischboeck.scrabbleator.engine;

import lombok.Getter;
import lombok.ToString;
import net.fischboeck.scrabbleator.model.BoardField;
import net.fischboeck.scrabbleator.model.ScrabbleBoard;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static net.fischboeck.scrabbleator.model.WordLayout.Direction;

/**
 * The {@link Window} is the 'brain' part of the AI-Engine.
 * A window can be placed somewhere on the {@link ScrabbleBoard} and then be expanded until it hits pre defined constraints.
 * It tries to cover as much {@link BoardField}s as possible to find spaces on the board where words can be laid out.
 * While expanding the window checks it's neighbor rows/columns and the boards boundaries.
 * The idea behind expansion is:
 * <pre>
 * 1) Starting at a given position (startPos) in a certain column/row (centerIndex)
 * 2) Check if the given start position is valid by>
 * 2.1) Check if the field it focusses on is either empty (+) or it already contains a stone.
 *      If it is empty, check if the neighbor fields in vertical/horizontal direction are also empty -> valid, or
 *      if they contain a stone -> invalid. In that case The window can't expand any further and will result in a
 *      zero sized length, which renders the window impossible to layout a word in it.
 * 2.2) If the starting position is valid, try to expand in the specified direction by checking if:
 *   a) Are the boards boundaries exceeded while doing expansion -> can't expand
 *   b) Expand by the rule described in 2.1
 * </pre>
 *
 * After this steps the window is expanded to a maximum size. The questions now are:
 * a) Does the window span over a {@link net.fischboeck.scrabbleator.model.Stone} that is already laid out on the board?
 *    If not the window can't be used to layout a word, since any layout must be placed where there is already a stone
 *    present on the board.
 * b) Can the window be split up into multiple windows? This is done by using regex on the {@link #getPattern()} that
 *    denotes the fields on the board the window is spanning.
 *
 * @author M.Fischböck
 */
@ToString(exclude = "board")
public class Window {

    private static final Pattern splitPattern = Pattern.compile("[A-Z]\\+{2,}[A-Z]");

    private static final Pattern centerSplitPattern = Pattern.compile("\\+*[A-Z]+\\+{2,}[A-Z]+\\+*");

    private static final Pattern rightSplittPattern = Pattern.compile("\\+[A-Z]*\\+{2,}$");

    private static final Pattern leftSplitPattern = Pattern.compile("\\+*[A-Z]*\\+{2,}");

    @Getter
    private int startPos;

    @Getter
    private int endPos;

    @Getter
    private int centerIndex;

    @Getter
    private Direction orientation;

    private ScrabbleBoard board;


    /**
     * Creates a new scanning window for the given coordinates
     * @param board The {@link ScrabbleBoard} the window is created within
     * @param startPos The starting position (0-based) the window starts at
     * @param center The index of the row/column the window is surrounding
     * @param orientation The orientation of the window
     */
    public Window(ScrabbleBoard board, int startPos, int center, Direction orientation) {
        this.board = board;
        this.startPos = startPos;
        this.endPos = startPos;
        this.centerIndex = center;
        this.orientation = orientation;
    }


    /**
     * Constructor to be used when a window is being split.
     * @param board The {@link ScrabbleBoard} the window is valid on
     * @param startPos The starting position of the window
     * @param endPos The stop position of the window (endPos - startPos == length)
     * @param center The index of the row / column the window is surrounding
     * @param orientation The orientation of the window
     */
    Window(ScrabbleBoard board, int startPos, int endPos, int center, Direction orientation) {
        this.board = board;
        this.startPos = startPos;
        this.endPos = endPos;
        this.centerIndex = center;
        this.orientation = orientation;

        if (startPos > endPos) {
            System.err.println(this.toString());
            throw new IllegalStateException("Invalid window size");
        }
    }


    /**
     * Expands the window to it's maximum size.
     * If the window is expanded it spans over a part of a column / row
     *
     * @return The new end position the window has spanned over
     */
    public final int expand() {

        if (orientation == Direction.Horizontal) {
            while (canExpandHorizontal()) {
                endPos++;
            }
            return this.endPos;
        }

        while (canExpandVertical()) {
            endPos++;
        }
        return this.endPos;
    }


    /**
     * Checks if the window can be expanded in horizontal manner
     * @return True if so, false otherwise
     */
    private boolean canExpandHorizontal() {

        // check if the current starting position is valid at all
        BoardField field = board.fieldAt(startPos, centerIndex);
        BoardField nextUp = board.nextUp(field);
        BoardField nextDown = board.nextDown(field);

        if (nextUp != null && !nextUp.isEmpty())
            return false;

        if (nextDown != null && !nextDown.isEmpty())
            return false;

        // we also need to check if there is already an occupied field to the left of startingPos.
        // If the field is occupied we return immediately leaving a zero sized window which will
        // be sorted out.
        if (this.startPos > 0) {
            BoardField nextLeft = board.fieldAt(startPos - 1, centerIndex);
            if (!nextLeft.isEmpty())
                return false;
        }

        int newEnd = endPos + 1;
        if (newEnd >= 15)
            return false;

        field = board.fieldAt(newEnd, centerIndex);

        if (!field.isEmpty())
            return true;

        nextUp = board.nextUp(field);
        nextDown = board.nextDown(field);

        // up and down are not null but both are empty -> ok
        if (nextUp != null && nextUp.isEmpty() && nextDown != null && nextDown.isEmpty())
            return true;

        // down is available and empty -> ok
        if (nextUp == null && nextDown != null && nextDown.isEmpty())
            return true;

        // up is available and empty -> ok
        if (nextDown == null && nextUp != null && nextUp.isEmpty())
            return true;

        return false;
    }


    /**
     * Returns whether or not the window can be expanded in {@link Direction#Vertical}.
     *
     * @return True if so, false otherwise
     */
    private boolean canExpandVertical() {

        // check if the current starting position is valid at all
        BoardField field = board.fieldAt(centerIndex, startPos);
        BoardField nextLeft = board.nextLeft(field);
        BoardField nextRight = board.nextRight(field);

        if (nextLeft != null && !nextLeft.isEmpty())
            return false;

        if (nextRight != null && !nextRight.isEmpty())
            return false;

        if (this.startPos > 0) {
            BoardField nextUp = board.fieldAt(startPos -1, centerIndex);
            if (!nextUp.isEmpty())
                return false;
        }

        int newEnd = endPos + 1;
        if (newEnd >= 15)
            return false;

        field = board.fieldAt(centerIndex, newEnd);

        if (!field.isEmpty())
            return true;

        nextLeft = board.nextLeft(field);
        nextRight = board.nextRight(field);

        if (nextLeft != null && nextLeft.isEmpty() && nextRight != null && nextRight.isEmpty())
            return true;

        if (nextLeft == null && nextRight != null && nextRight.isEmpty())
            return true;

        if (nextRight == null && nextLeft != null && nextLeft.isEmpty())
            return true;

        return false;
    }


    /**
     * Returns whether or not the window spans over zero {@link BoardField}s
     * If a window has a length of zero, it is not possible to layout a new word in this window
     *
     * @return True if so, false otherwise.
     */
    public boolean isZeroSized() {
        return startPos == endPos;
    }


    /**
     * Returns whether or not the window spans over {@link BoardField}s in such a way that it contains multiple
     * valid windows.
     *
     * @return True if the window can be split, false otherwise
     */
    public boolean canSplit() {
        String testString = getPattern();
        Matcher m = centerSplitPattern.matcher(testString);
        return m.matches();

        // TODO: There is another way of splitting. Implement it!
        // From: KLUFT+E++++ the '+E++++' part can be extracted and of course the other way round
    }

    /**
     * Splits the given window into multiple windows, where applicable.
     * The originated window will not be affected by this operation.
     *
     * @return The list of new {@link Window}s that are yielded from this operation.
     */
    public List<Window> split() {

        List<Window> windows = new ArrayList<>();
        Matcher m = splitPattern.matcher(getPattern());

        if (m.find()) {

            // left partial
            windows.add(new Window(board,
                    this.startPos,
                    this.startPos + m.end(0) - 3,
                    this.centerIndex, orientation));

            // right partial
            windows.add(new Window(board,
                    this.startPos + m.start(0) + 2,
                    this.endPos,
                    this.centerIndex, orientation));
        }

        return windows;
    }

    /**
     * Returns whether or not the window spans over stones that are already laid out on the {@link ScrabbleBoard}.
     *
     * @return True if the window spans accross laid out {@link net.fischboeck.scrabbleator.model.Stone}s. false otherwise
     */
    public boolean containsStones() {

        long occupied = fieldsOf(board)
                .stream()
                .filter(field -> !field.isEmpty())
                .count();
        return occupied != 0;
    }


    /**
     * Returns the string pattern of {@link BoardField}s the window spans across in it's center row/column.
     * The pattern is denoted in the following way:
     * 'A-Z' : A field containing a laid out stone with the given letter
     * '+' : A field not containing a stone yet
     *
     * @return The pattern
     */
    public String getPattern() {
        List<BoardField> fields = fieldsOf(board);

        return fields.stream()
                .map(field -> field.isEmpty()? "+" : Character.toString(field.getStone().getLetter()))
                .collect(Collectors.joining());
    }


    private List<BoardField> fieldsOf(ScrabbleBoard board) {
        if (orientation == Direction.Horizontal)
            return board.fieldsHorizontal(startPos, centerIndex, endPos - startPos + 1);

        return board.fieldsVertical(centerIndex, startPos, endPos - startPos + 1);
    }
}
