package net.fischboeck.scrabbleator.engine;

import net.fischboeck.scrabbleator.model.*;

/**
 * @author M.Fischböck
 */
public class WordLayoutScoreCalculator {

    public static int calculateScore(ScrabbleBoard board, StoneBank bank, WordLayout layout) {

        int bankBonus = calculateAllFromBankBonus(bank, layout);
        int wordScore = calculateWordScore(board, layout);
        wordScore = applyWordMultiplicators(board, layout, wordScore);

        return wordScore + bankBonus;
    }


    static int calculateAllFromBankBonus(StoneBank bank, WordLayout layout) {

        // bonus if the word is laid from all bank letters
        int fromBankCount = (int) layout.getStones().stream()
                .filter(stone -> bank.contains(stone))
                .count();

        if (fromBankCount == 0)
            return 0;

        if (fromBankCount == bank.size()) {
            return 50;
        }

        return 0;
    }


    static int calculateWordScore(ScrabbleBoard board, WordLayout layout) {

        int score = 0;

        BoardField field = layout.getStartingField();
        for (int i=0; i < layout.getStones().size(); i++) {

            Stone stone = layout.getStones().get(i);

            // check if the stone is already placed on the field
            if (field.getStone() != null && stone.equals(field.getStone())) {

                // no bonus - only single stone value
                score += stone.getValue();
            } else {

                // stone is not placed on the field... add eventual bonus
                int value = stone.getValue();

                if (field.getMultiplicator() == BoardField.Multiplicator.LETTER_TIMES_2)
                    value = value * 2;

                if (field.getMultiplicator() == BoardField.Multiplicator.LETTER_TIMES_3)
                    value = value * 3;

                score += value;
            }

            // next field if we have any more
            if (i+1 < layout.getStones().size()) {
                field = board.nextNeighbor(field, layout.getLayoutDirection());
                if (field == null) {
                    System.err.print(layout.toString());
                    throw new IllegalStateException("Word doesn't fit on board. Check with WordLayoutValidator first");
                }
            }
        }

        return score;
    }


    static int applyWordMultiplicators(ScrabbleBoard board, WordLayout layout, int wordScore) {

        BoardField field = layout.getStartingField();
        for (int i=0; i < layout.getStones().size(); i++) {

            Stone stone = layout.getStones().get(i);

            if (field.getStone() != null && stone.equals(field.getStone())) {

                // noop -> stone is already placed on this field

            } else {

                // check if the field contains a word multiplicator
                if (field.getMultiplicator() == BoardField.Multiplicator.WORD_TIMES_2)
                    wordScore = wordScore * 2;

                if (field.getMultiplicator() == BoardField.Multiplicator.WORD_TIMES_3)
                    wordScore = wordScore * 3;
            }

            // next field
            if (i+1 < layout.getStones().size()) {
                field = board.nextNeighbor(field, layout.getLayoutDirection());
                if (field == null)
                    throw new IllegalStateException("Word doesn't fit on board. Check with WordLayoutValidator first");
            }
        }

        return wordScore;
    }
}
