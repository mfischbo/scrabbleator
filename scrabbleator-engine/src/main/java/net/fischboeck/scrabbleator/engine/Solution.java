package net.fischboeck.scrabbleator.engine;

import lombok.Getter;
import net.fischboeck.scrabbleator.model.WordLayout;

import java.util.Set;

/**
 * Data container that holds a valid {@link Window} and a set of possible {@link WordLayout}s for that window.
 *
 * @author M.Fischböck
 */
@Getter
public class Solution {

    private Window window;
    private Set<WordLayout> layouts;


    public Solution(Window window, Set<WordLayout> layouts) {
        this.window = window;
        this.layouts = layouts;
    }
}
