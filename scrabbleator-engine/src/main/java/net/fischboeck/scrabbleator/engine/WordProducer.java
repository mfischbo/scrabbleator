package net.fischboeck.scrabbleator.engine;

import net.fischboeck.scrabbleator.model.*;

import java.util.*;
import java.util.regex.Pattern;

/**
 * @author M.Fischböck
 */
public class WordProducer {

    private static final Pattern pattern = Pattern.compile("[A-Z]*");

    private Set<String> dictionary;

    public WordProducer(Set<String> dictionary) {
        this.dictionary = dictionary;
    }


    public Set<String> findWordsMatchingLetters(String letters) {

        if (letters == null || letters.length() == 0)
            throw new IllegalArgumentException("Input was null or empty");

        if (!letters.matches("[A-Z]*"))
            throw new IllegalArgumentException("Can only produce words for letters A-Z");

        Set<String> retval = new HashSet<>();
        for (String word : dictionary) {

            // skip words that are longer than input characters
            if (word.length() > letters.length())
                continue;

            // skip words that contain characters other than those in the chars array
            if (!containsOnly(word, letters))
                continue;

            retval.add(word);
        }
        return retval;
    }


    /**
     * Returns all words matching the specified pattern containing characters from the letters string.
     * The pattern is constructed out of the following tokens:
     * [LETTER] The letter is fixed and must occur at the specified position
     * + The word can contain a letter from the input letters at the specified position.
     *
     * Examples:
     * B+++ - 4 letter (or shorter) word containing a "B" at index 0
     * ++B++ - 5 letter word (or shorter)
     *
     * @return A set of words that matches the pattern and input letters
     */
    public Set<WordLayout> findWordsMatchingPattern(ScrabbleBoard board, StoneBank bank, Window window) {

        String letters = bank.toLetterString();

        if (bank == null || bank.size() == 0)
            throw new IllegalArgumentException("Input was null or empty");

        if (!letters.matches("[A-Z]*"))
            throw new IllegalArgumentException("Can only produce words for letters A-Z");


        Set<WordLayout> retval = new HashSet<>();

        // find all words matching the pattern as is
        int startingPosOffset = 0;
        String pattern = window.getPattern();

        Set<String> solutions = matchPattern(pattern, letters);
        if (solutions.size() > 0)
            retval.addAll(createLayouts(board, window, bank, window.getPattern(), solutions, startingPosOffset));


        // gradually reduce the pattern from the left and the right side to find more matches
        while (canReduceLeft(pattern) || canReduceRight(pattern)) {

            if (canReduceRight(pattern)) {
                pattern = pattern.substring(0, pattern.length() -1);
            } else if (canReduceLeft(pattern)) {
                pattern = pattern.substring(1);
                startingPosOffset++;
            }

            solutions = matchPattern(pattern, letters);
            if (solutions.size() > 0)
                retval.addAll(createLayouts(board, window, bank, pattern, solutions, startingPosOffset));
        }
        return retval;
    }


    private Set<String> matchPattern(String pattern, String letters) {

        Map<Integer, Character> fixedChars = new HashMap<>();
        for (int i=0; i < pattern.length(); i++) {
            if (pattern.charAt(i) != '+')
                fixedChars.put(i, pattern.charAt(i));
        }

        Set<String> retval = new HashSet<>();

        // find all words that have the specified fixed characters
        withNextWord:
        for (String word : dictionary) {

            if (word.length() != pattern.length())
                continue withNextWord;

            char[] wordChars = word.toCharArray();

            // check if the word has characters at the fixed positions
            for (Map.Entry<Integer, Character> e : fixedChars.entrySet()) {
                if (word.charAt(e.getKey()) != e.getValue())
                    continue withNextWord;

                // mask the char for later removal
                wordChars[e.getKey()] =  '-';
            }

            // check if the blanks can be filled with characters from the input array
            // therefore remove all characters at the fixed positions from the word
            String remaining = new String(wordChars).replaceAll("-", "");
            if (remaining.length() == 0)
                continue withNextWord;

            // and check if the remaining blanks can be filled with the input letters
            if (!containsOnly(remaining, letters))
                continue withNextWord;

            // if so, we have a match
            retval.add(word);
        }

        return retval;
    }

    private Set<WordLayout> createLayouts(ScrabbleBoard board, Window window, StoneBank bank,
                                          String searchPattern, Set<String> solutions, int startPosOffset) {

        Set<WordLayout> layouts = new HashSet<>();

        for (String solution : solutions) {
            layouts.add(createWordLayout(board, window, bank, searchPattern, solution, startPosOffset));
        }
        return layouts;
    }

    private WordLayout createWordLayout(
            ScrabbleBoard board, Window window, StoneBank bank,
            String searchPattern, String solution, int startPosOffset) {

        // get the field the word layout will start
        BoardField field = null;
        if (window.getOrientation() == WordLayout.Direction.Horizontal) {
            field = board.fieldAt(window.getStartPos() + startPosOffset, window.getCenterIndex());
        } else {
            field = board.fieldAt(window.getCenterIndex(), window.getStartPos() + startPosOffset);
        }

        // get the list of stones that will be removed from the players bank
        List<Stone> stones = new ArrayList<>();
        for (int i=0; i < solution.length(); i++) {

            // pick one from the bank
            if (searchPattern.charAt(i) == '+') {
                char toDraw = solution.charAt(i);

                if (!bank.containsChar(toDraw)) {
                    throw new IllegalStateException("Tried to create a word layout with a stone not on the bank");
                } else {
                    stones.add(bank.pick(toDraw));
                }
            } else {

                // fixed stone already on the field -> Find it
                BoardField fixed = null;
                if (window.getOrientation() == WordLayout.Direction.Horizontal) {
                    int x = window.getStartPos() + startPosOffset + i;
                    fixed = board.fieldAt(x, window.getCenterIndex());
                } else {
                    int y = window.getStartPos() + startPosOffset + i;
                    fixed = board.fieldAt(window.getCenterIndex(), y);
                }

                if (fixed == null)
                    throw new IllegalStateException("The fixed stone on the board could not be found");

                stones.add(fixed.getStone());
            }
        }

        WordLayout layout = new WordLayout(field, window.getOrientation(), stones);
        int score = WordLayoutScoreCalculator.calculateScore(board, bank, layout);
        layout.setScore(score);
        return layout;
    }


    /**
     * Returns whether or not a pattern can be reduced from the left side without removing fixed characters
     * @param pattern The pattern to be checked
     * @return True if the pattern can be reduced, false otherwise
     */
    private boolean canReduceLeft(String pattern) {

        // the pattern must contain at least 3 letters
        if (pattern.length() < 3) {
            return false;
        }

        // the pattern must contain a + at index 0 or at index length-1
        if (pattern.charAt(0) != '+')
            return false;

        return true;
    }

    /**
     * Returns whether or not a pattern can be reduced from the right side without removing fixed characters
     * @param pattern The pattern to be checked
     * @return True if the pattern can be reduced, false otherwise
     */
    private boolean canReduceRight(String pattern) {

        if (pattern.length() < 3)
            return false;

        if (pattern.charAt(pattern.length()-1) != '+')
            return false;

        return true;
    }




    private boolean containsOnly(String word, String input) {

        List<String> wordChars = Arrays.asList(word.split(""));
        ArrayList<String> characters = new ArrayList<>();
        characters.addAll(Arrays.asList(input.split("")));

        for (String wc : wordChars) {

            int idx = characters.indexOf(wc);

            if (idx == -1)
                return false;

            characters.remove(idx);
        }
        return true;
    }
}
