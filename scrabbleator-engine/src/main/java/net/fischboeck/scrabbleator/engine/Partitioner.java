package net.fischboeck.scrabbleator.engine;

import net.fischboeck.scrabbleator.model.ScrabbleBoard;

import java.util.ArrayList;
import java.util.List;

import static net.fischboeck.scrabbleator.model.WordLayout.Direction;
import static net.fischboeck.scrabbleator.model.WordLayout.Direction.Horizontal;
import static net.fischboeck.scrabbleator.model.WordLayout.Direction.Vertical;

/**
 * The partitioner tries to find {@link Window}s where new words can be placed at given the current {@link ScrabbleBoard}
 *
 * @author M.Fischböck
 */
public class Partitioner {

    private final ScrabbleBoard board;
    private final int centerIndex;

    /**
     * Constructs a new Partitioner for the given {@link ScrabbleBoard} around the specified center row / column.
     *
     * @param board The board we are playing on
     * @param centerIndex The index of the center the produced {@link Window}s will have (either row or columns).
     */
    public Partitioner(final ScrabbleBoard board, final int centerIndex) {
        this.board = board;
        this.centerIndex = centerIndex;
    }

    /**
     * Returns a list of {@link Window}s for the specified {@link Direction}.
     *
     * @param direction The direction in which to partition the board.
     * @return The list of valid windows for the center index. Can be empty but never <code>null</code>
     */
    public List<Window> findPartitions(Direction direction) {

        List<Window> retval = new ArrayList<>();

        // see if the current row has any stones on the field
        long occupiedFields = board.fieldsHorizontal(0, centerIndex, 15).stream()
                .filter(field -> field.getStone() != null)
                .count();

        // nope? This is not a partition to lay stones into
        if (occupiedFields == 0)
            return retval;


        int lastEndPos = 0;
        Window window = new Window(board, 0, centerIndex, direction);
        while (lastEndPos < 14) {

            lastEndPos = window.expand();

            if (!window.isZeroSized() && window.containsStones()) {
                retval.add(window);

                // check if the window can be split
                if (window.canSplit()) {
                    retval.addAll(window.split());
                }

                window = new Window(board, lastEndPos, centerIndex, direction);
            } else {
                window = new Window(board, lastEndPos + 1, centerIndex, direction);
            }
        }
        return retval;
    }


    /**
     * Shorthand method to find all {@link Window}s in either direction.
     *
     * @return A list of windows. Can be empty but never null.
     */
    public List<Window> findAllPartitions() {
        List<Window> retval = findPartitions(Horizontal);
        retval.addAll(findPartitions(Vertical));
        return retval;
    }
}
