package net.fischboeck.scrabbleator.states;

import net.fischboeck.scrabbleator.model.GameContext;

/**
 * @author M.Fischböck
 */
public interface IGameState {

    /**
     * Advances the provided {@link GameContext} to the next state based on the current state of the game
     *
     * @param context The context to be advanced
     */
    void nextState(GameContext context);
}
