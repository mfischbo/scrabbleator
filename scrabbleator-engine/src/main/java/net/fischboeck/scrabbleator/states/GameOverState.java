package net.fischboeck.scrabbleator.states;

import net.fischboeck.scrabbleator.model.GameContext;

/**
 * @author M.Fischböck
 */
public class GameOverState implements IGameState {

    @Override
    public void nextState(GameContext context) {

    }
}
