package net.fischboeck.scrabbleator.states;

import net.fischboeck.scrabbleator.model.GameContext;
import net.fischboeck.scrabbleator.model.Player;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author M.Fischböck
 */
public class DecideWinnerState implements IGameState {

    @Override
    public void nextState(GameContext context) {

        // check the players score boards and decide who has won the game
        List<Player> playersByScore = context.getPlayers().getPlayersByScore();

        // player at index 0 has the highest score... see if others have that score as well
        int highscore = playersByScore.get(0).getScore();

        List<Player> winners = playersByScore.stream()
                .filter(p -> p.getScore() == highscore)
                .collect(Collectors.toList());

        context.setWinners(winners);
        context.setGameState(new GameOverState());
    }
}
