package net.fischboeck.scrabbleator.states;

import net.fischboeck.scrabbleator.model.GameContext;
import net.fischboeck.scrabbleator.model.Player;

/**
 * @author M.Fischböck
 */
public class AfterFindWordState implements IGameState {

    private boolean playerPassed;

    public AfterFindWordState(boolean playerPassed) {
        this.playerPassed = playerPassed;
    }

    @Override
    public void nextState(GameContext context) {

        // check if the game is over:
        // a) Bag is empty and at least one player has 0 stones on the bank
        // b) All players passed 2 times
        boolean allPassedTwice = true;

        Player anchor = context.getCurrentPlayer();
        Player next = context.getPlayers().getNextClockwise(anchor);

        while (next.equals(anchor)) {

            // check if the bag is empty and the players bank counts 0
            if (context.getBag().size() == 0 && next.getStoneBank().size() == 0) {
                context.setGameState(new DecideWinnerState());
                return;
            }

            // check if the player passed twice:
            allPassedTwice = allPassedTwice && (next.getPassingStreak() == 2);

            // next player
            next = context.getPlayers().getNextClockwise(next);
        }

        if (allPassedTwice) {
            context.setGameState(new DecideWinnerState());
        }


        // game has not ended:
        context.setCurrentPlayer(context.getPlayers().getNextClockwise(anchor));
        context.setGameState(new FindWordState());
    }
}
