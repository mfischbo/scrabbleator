package net.fischboeck.scrabbleator.states;

import lombok.Setter;
import net.fischboeck.scrabbleator.engine.WordDictionaryValidator;
import net.fischboeck.scrabbleator.engine.WordLayoutScoreCalculator;
import net.fischboeck.scrabbleator.engine.WordLayoutValidator;
import net.fischboeck.scrabbleator.model.*;

import java.util.List;

/**
 * @author M.Fischböck
 */
public class FindWordState implements IGameState {

    @Setter
    private WordLayout wordLayout;

    @Setter
    private List<Stone> exchangeStones;


    @Override
    public void nextState(GameContext context) {

        // current player wants to lay out a word
        if (wordLayout != null) {

            // check if the word layout is valid (e.g. it's on the bounds of the game board)
            boolean valid = isValidLayout(context.getBoard(), wordLayout);
            boolean validWord = isValidWord(context.getBoard(), wordLayout);

            if (valid && validWord) {

                // calculate the score for the word layout and add it to the players score
                Player current = context.getCurrentPlayer();

                int score = calculateWordScore(context.getBoard(), current.getStoneBank(), wordLayout);
                current.addScore(score);

                // set the passingStreak count to 0
                current.setPassingStreak(0);

                // remove the laid out stones from the players bank
                current.getStoneBank().remove(wordLayout.getStones());

                // pick new stones from the bag and add it to the players bank
                int amount = 7 - current.getStoneBank().size();
                List<Stone> newPicks = context.getBag().pickRandomly(amount);
                current.getStoneBank().add(newPicks);

                // advance the game
                context.setGameState(new AfterFindWordState(false));
                return;

            } else {
                context.setGameState(new InvalidMoveState(valid, validWord));
            }

        } else {

            // current player passes.
            Player current = context.getCurrentPlayer();
            current.setPassingStreak(current.getPassingStreak() + 1);

            // player passes without picking new stones from the bag
            if (exchangeStones == null || exchangeStones.isEmpty()) {
                context.setGameState(new AfterFindWordState(true));
                return;

            } else {

                // player puts back stones in the bag and picks new ones
                int amount = this.exchangeStones.size();
                current.getStoneBank().remove(this.exchangeStones);
                context.getBag().addAll(this.exchangeStones);
                current.getStoneBank().add(context.getBag().pickRandomly(amount));
                context.setGameState(new AfterFindWordState(true));
                return;
            }
        }
    }


    private boolean isValidLayout(ScrabbleBoard board, WordLayout layout) {
        return WordLayoutValidator.isValid(board, layout);
    }

    private boolean isValidWord(ScrabbleBoard board, WordLayout layout) {
        return WordDictionaryValidator.isValidWord(board, layout);
    }

    private int calculateWordScore(ScrabbleBoard board, StoneBank bank, WordLayout layout) {
        return WordLayoutScoreCalculator.calculateScore(board, bank, layout);
    }
}
