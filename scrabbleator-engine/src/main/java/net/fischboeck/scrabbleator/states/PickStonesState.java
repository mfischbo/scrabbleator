package net.fischboeck.scrabbleator.states;

import net.fischboeck.scrabbleator.model.GameContext;
import net.fischboeck.scrabbleator.model.Player;
import net.fischboeck.scrabbleator.model.Stone;

import java.util.List;

/**
 * @author M.Fischböck
 */
public class PickStonesState implements IGameState {


    @Override
    public void nextState(GameContext context) {

        List<Stone> stones = context.getBag().pickRandomly(7);
        context.getCurrentPlayer().getStoneBank().add(stones);
        context.getCurrentPlayer().setPerformedInitialPick(true);

        // check if the next player has already picked stones
        Player next = context.getPlayers().getNextClockwise(context.getCurrentPlayer());
        if (!next.isPerformedInitialPick()) {
            context.setCurrentPlayer(next);
            context.setGameState(new PickStonesState());
        } else {
            context.setCurrentPlayer(next);
            context.setGameState(null);
        }
    }
}
