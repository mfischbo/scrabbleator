package net.fischboeck.scrabbleator.states;

import net.fischboeck.scrabbleator.model.GameContext;

import java.util.Random;

/**
 * Randomly picks the current player from the list of players
 * @author M.Fischböck
 */
public class DecideFirstPlayerState implements IGameState {


    @Override
    public void nextState(GameContext context) {

        Random rnd = new Random();
        int index = rnd.nextInt(context.getPlayers().size());
        context.setCurrentPlayer(context.getPlayers().at(index));
        context.setGameState(new PickStonesState());
    }
}
