package net.fischboeck.scrabbleator.states;

import net.fischboeck.scrabbleator.model.GameContext;

/**
 * @author M.Fischböck
 */
public class InvalidMoveState implements IGameState {

    private boolean ruleConflict;
    private boolean invalidWord;

    public InvalidMoveState(boolean ruleConflict, boolean invalidWord) {
        this.ruleConflict = ruleConflict;
        this.invalidWord = invalidWord;
    }

    @Override
    public void nextState(GameContext context) {

        // go back to FindWordState and wait for player input
        context.setGameState(new FindWordState());
    }
}
