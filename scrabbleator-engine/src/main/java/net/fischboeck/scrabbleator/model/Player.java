package net.fischboeck.scrabbleator.model;

import lombok.Data;

/**
 * Represents a player in the current game.
 *
 * @author M.Fischböck
 */
@Data
public class Player {

    private String name;

    private StoneBank stoneBank;

    private int score;

    /**
     * How many times did the player pass in a row.
     * If all players passed 2 times, the game is finished.
     * This should be reset to 0 if the player lays out a word
     */
    private int passingStreak;

    /**
     * States if the player had initially picked is stones
     */
    private boolean performedInitialPick;

    public Player(String name) {
        this.name = name;
    }

    public void addScore(int score) {
        if (score < 0)
            throw new IllegalArgumentException("Can not add negative score");
        this.score += score;
    }
}
