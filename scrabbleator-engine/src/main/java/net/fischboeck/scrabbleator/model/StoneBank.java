package net.fischboeck.scrabbleator.model;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Represents the board of stones each player has.
 * @author M.Fischböck
 */
public class StoneBank {

    private List<Stone> stones;

    public StoneBank() {
        this.stones = new ArrayList<>();
    }

    /**
     * Adds the provided stones to the players board.
     *
     * @param stones
     * @throws IllegalStateException If the amount of stones exeeds 7 or -1
     */
    public void add(List<Stone> stones) throws IllegalStateException {

        int sum = stones.size() + this.stones.size();
        if (sum > 7)
            throw new IllegalStateException("The amount of stones must not exeed 7");

        this.stones.addAll(stones);
    }

    public Stone pick(Character character) {
        for (Stone s : stones) {
            if (s.getLetter() == character)
                return s;
        }
        return null;
    }

    public void remove(List<Stone> stones) {
        this.stones.removeAll(stones);
    }

    public boolean containsChar(char character) {
        for (Stone s : stones) {
            if (s.getLetter() == character)
                return true;
        }
        return false;
    }


    public Stone removeByCharacter(char character) {
        Iterator<Stone> sit = stones.iterator();
        while (sit.hasNext()) {
            Stone retval = sit.next();
            if (retval.getLetter() == character) {
                sit.remove();
                return retval;
            }
        }
        return null;
    }


    public String toLetterString() {
        return stones.stream()
                .map(stone -> Character.toString(stone.getLetter()))
                .collect(Collectors.joining());
    }

    public int size() { return this.stones.size(); }

    /**
     * Returns true if the given stone is on the bank
     *
     * @param stone The stone to be checked whether it is on the bank or not
     * @return True if so, false otherwise
     */
    public boolean contains(Stone stone) {
        return stones.contains(stone);
    }
}
