package net.fischboeck.scrabbleator.model;

import lombok.Data;

/**
 * @author M.Fischböck
 */
@Data
public class BoardField {

    public enum Multiplicator {
        WORD_TIMES_2,
        WORD_TIMES_3,
        LETTER_TIMES_2,
        LETTER_TIMES_3
    }

    private Stone stone;

    private Multiplicator multiplicator;


    public boolean isEmpty() {
        return stone == null;
    }
}
