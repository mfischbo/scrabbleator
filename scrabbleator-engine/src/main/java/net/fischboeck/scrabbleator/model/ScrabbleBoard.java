package net.fischboeck.scrabbleator.model;

import lombok.AllArgsConstructor;

import java.util.ArrayList;
import java.util.List;

import static net.fischboeck.scrabbleator.model.WordLayout.Direction.Horizontal;

/**
 * @author M.Fischböck
 */
public class ScrabbleBoard {

    private BoardField[][] board;

    public ScrabbleBoard() {
        this.board = new BoardField[15][15];

        for (int x=0; x < 15; x++) {
            for (int y=0; y < 15; y++) {
                this.board[x][y] = new BoardField();
            }
        }

        // set the multiplicators
        // row 1
        this.board[0][0].setMultiplicator(BoardField.Multiplicator.WORD_TIMES_3);
        this.board[3][0].setMultiplicator(BoardField.Multiplicator.LETTER_TIMES_2);
        this.board[7][0].setMultiplicator(BoardField.Multiplicator.WORD_TIMES_3);
        this.board[11][0].setMultiplicator(BoardField.Multiplicator.LETTER_TIMES_2);
        this.board[14][0].setMultiplicator(BoardField.Multiplicator.WORD_TIMES_3);

        // row 2
        this.board[1][1].setMultiplicator(BoardField.Multiplicator.WORD_TIMES_2);
        this.board[5][1].setMultiplicator(BoardField.Multiplicator.LETTER_TIMES_3);
        this.board[9][1].setMultiplicator(BoardField.Multiplicator.LETTER_TIMES_3);
        this.board[13][1].setMultiplicator(BoardField.Multiplicator.WORD_TIMES_2);

        // row 3
        this.board[2][2].setMultiplicator(BoardField.Multiplicator.WORD_TIMES_2);
        this.board[6][2].setMultiplicator(BoardField.Multiplicator.LETTER_TIMES_2);
        this.board[8][2].setMultiplicator(BoardField.Multiplicator.LETTER_TIMES_2);
        this.board[12][2].setMultiplicator(BoardField.Multiplicator.WORD_TIMES_2);

        // row 4
        this.board[0][3].setMultiplicator(BoardField.Multiplicator.LETTER_TIMES_2);
        this.board[3][3].setMultiplicator(BoardField.Multiplicator.WORD_TIMES_2);
        this.board[7][3].setMultiplicator(BoardField.Multiplicator.LETTER_TIMES_2);
        this.board[11][3].setMultiplicator(BoardField.Multiplicator.WORD_TIMES_2);
        this.board[14][3].setMultiplicator(BoardField.Multiplicator.LETTER_TIMES_2);

        // row 5
        this.board[4][4].setMultiplicator(BoardField.Multiplicator.WORD_TIMES_2);
        this.board[10][4].setMultiplicator(BoardField.Multiplicator.WORD_TIMES_2);

        // row 6
        this.board[1][5].setMultiplicator(BoardField.Multiplicator.WORD_TIMES_3);
        this.board[5][5].setMultiplicator(BoardField.Multiplicator.WORD_TIMES_3);
        this.board[9][5].setMultiplicator(BoardField.Multiplicator.WORD_TIMES_3);
        this.board[13][5].setMultiplicator(BoardField.Multiplicator.WORD_TIMES_3);

        // row 7
        this.board[2][6].setMultiplicator(BoardField.Multiplicator.LETTER_TIMES_2);
        this.board[6][6].setMultiplicator(BoardField.Multiplicator.LETTER_TIMES_2);
        this.board[8][6].setMultiplicator(BoardField.Multiplicator.LETTER_TIMES_2);
        this.board[12][6].setMultiplicator(BoardField.Multiplicator.LETTER_TIMES_2);

        // row 8
        this.board[0][7].setMultiplicator(BoardField.Multiplicator.WORD_TIMES_3);
        this.board[3][7].setMultiplicator(BoardField.Multiplicator.LETTER_TIMES_2);
        this.board[11][7].setMultiplicator(BoardField.Multiplicator.LETTER_TIMES_2);
        this.board[14][7].setMultiplicator(BoardField.Multiplicator.WORD_TIMES_3);

        // row 9
        this.board[2][8].setMultiplicator(BoardField.Multiplicator.LETTER_TIMES_2);
        this.board[6][8].setMultiplicator(BoardField.Multiplicator.LETTER_TIMES_2);
        this.board[8][8].setMultiplicator(BoardField.Multiplicator.LETTER_TIMES_2);
        this.board[12][8].setMultiplicator(BoardField.Multiplicator.LETTER_TIMES_2);

        // row 10
        this.board[1][9].setMultiplicator(BoardField.Multiplicator.WORD_TIMES_3);
        this.board[5][9].setMultiplicator(BoardField.Multiplicator.WORD_TIMES_3);
        this.board[9][9].setMultiplicator(BoardField.Multiplicator.WORD_TIMES_3);
        this.board[13][9].setMultiplicator(BoardField.Multiplicator.WORD_TIMES_3);

        // row 11
        this.board[4][10].setMultiplicator(BoardField.Multiplicator.WORD_TIMES_2);
        this.board[10][10].setMultiplicator(BoardField.Multiplicator.WORD_TIMES_2);

        // row 12
        this.board[0][11].setMultiplicator(BoardField.Multiplicator.LETTER_TIMES_2);
        this.board[3][11].setMultiplicator(BoardField.Multiplicator.WORD_TIMES_2);
        this.board[7][11].setMultiplicator(BoardField.Multiplicator.LETTER_TIMES_2);
        this.board[11][11].setMultiplicator(BoardField.Multiplicator.WORD_TIMES_2);
        this.board[14][11].setMultiplicator(BoardField.Multiplicator.LETTER_TIMES_2);

        // row 13
        this.board[2][12].setMultiplicator(BoardField.Multiplicator.WORD_TIMES_2);
        this.board[6][12].setMultiplicator(BoardField.Multiplicator.LETTER_TIMES_2);
        this.board[8][12].setMultiplicator(BoardField.Multiplicator.LETTER_TIMES_2);
        this.board[12][12].setMultiplicator(BoardField.Multiplicator.WORD_TIMES_2);

        // row 14
        this.board[1][13].setMultiplicator(BoardField.Multiplicator.WORD_TIMES_2);
        this.board[5][13].setMultiplicator(BoardField.Multiplicator.LETTER_TIMES_3);
        this.board[9][13].setMultiplicator(BoardField.Multiplicator.LETTER_TIMES_3);
        this.board[13][13].setMultiplicator(BoardField.Multiplicator.WORD_TIMES_2);

        // row 15
        this.board[0][14].setMultiplicator(BoardField.Multiplicator.WORD_TIMES_3);
        this.board[3][14].setMultiplicator(BoardField.Multiplicator.LETTER_TIMES_2);
        this.board[7][14].setMultiplicator(BoardField.Multiplicator.WORD_TIMES_3);
        this.board[11][14].setMultiplicator(BoardField.Multiplicator.LETTER_TIMES_2);
        this.board[14][14].setMultiplicator(BoardField.Multiplicator.WORD_TIMES_3);
    }


    /**
     * Returns the position of the specified board field on the board.
     * Returns <code>null</code> if the field is not part of the board at all.
     *
     * @param field The field
     * @return The Position (x, y) coordinates
     */
    public Position getPositionOf(final BoardField field) {
        for (int x=0; x < 15; x++) {
            for (int y=0; y < 15; y++) {
                if (board[x][y] == field) {
                    return new Position(x, y);
                }
            }
        }
        return null;
    }

    public BoardField nextNeighbor(final BoardField field, WordLayout.Direction direction) {
        if (direction == Horizontal)
            return nextRight(field);
        return nextDown(field);
    }

    /**
     * Returns the next right neighbor of the specified field.
     * Returns null if the fields position is at the right edge of the board.
     *
     * @param field The field to find the next right neighbor
     * @return The neighbor field or <code>null</code>
     */
    public BoardField nextRight(final BoardField field) {
        Position pos = getPositionOf(field);
        if (pos == null)
            return null;

        if (pos.x >= 14)
            return null;

        return board[pos.x+1][pos.y];
    }


    public BoardField nextLeft(final BoardField field) {
        Position pos = getPositionOf(field);
        if (pos == null)
            return null;

        if (pos.x == 0)
            return null;

        return board[pos.x-1][pos.y];
    }


    /**
     * Returns the field that is exactly one row above the given field
     * @param field
     * @return
     */
    public BoardField nextUp(final BoardField field) {
        Position pos = getPositionOf(field);
        if (pos == null)
            return null;

        if (pos.y == 0)
            return null;

        return board[pos.x][pos.y-1];
    }


    /**
     * Returns the next field that is in y+1 position.
     * Return null if the the specified board field is at the bottom edge of the board
     *
     * @param field
     * @return
     */
    public BoardField nextDown(final BoardField field) {
        Position pos = getPositionOf(field);
        if (pos == null)
            return null;

        if (pos.y >= 14)
            return null;

        return this.board[pos.x][pos.y+1];
    }

    public BoardField fieldAt(int x, int y) {
        if (x < 0 || y < 0)
            return null;

        if (x > 14 || y > 14)
            return null;

        return board[x][y];
    }


    public BoardField fieldAtChar(Character column, int row) {
        int x = ((int) column) -65; // ASCII Offset of 'A'
        return fieldAt(x, row - 1);
    }

    public BoardField fieldAt(Position position) {
        return fieldAt(position.x, position.y);
    }

    /**
     * Returns whether all fields do not contain stones
     *
     * @return
     */
    public boolean isEmpty() {

        for (int x=0; x < 15; x++) {
            for (int y=0; y < 15; y++) {
                if (board[x][y].getStone() != null)
                    return false;
            }
        }
        return true;
    }

    public List<BoardField> fieldsHorizontal(int x, int y, int length) {
        List<BoardField> retval = new ArrayList<>(length);
        for (int i=0; i < length; i++) {
            retval.add(board[x+i][y]);
        }
        return retval;
    }


    public List<BoardField> fieldsVertical(int x, int y, int length) {
        List<BoardField> retval = new ArrayList<>(length);
        for (int i=0; i < length; i++) {
            retval.add(board[x][y+i]);
        }
        return retval;
    }


    public void apply(WordLayout layout) {

        BoardField field = layout.getStartingField();
        for (Stone s : layout.getStones()) {
            field.setStone(s);
            field = nextNeighbor(field, layout.getLayoutDirection());
        }
    }


    @AllArgsConstructor
    public static final class Position {
        public int x;
        public int y;
    }
}
