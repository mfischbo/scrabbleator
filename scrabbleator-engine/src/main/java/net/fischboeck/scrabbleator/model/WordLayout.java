package net.fischboeck.scrabbleator.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author M.Fischböck
 */
@Getter
@ToString
public class WordLayout {

    public enum Direction {
        Horizontal, Vertical
    }

    // the word to layout
    private List<Stone> stones;

    // The field where the word starts
    private BoardField startingField;

    // The direction in which the word should be layed out
    private Direction layoutDirection;

    // The score this layout will achieve
    @Setter
    private int score;


    public WordLayout(BoardField startingField, Direction direction, List<Stone> stones) {
        if (startingField == null)
            throw new IllegalArgumentException("The starting field must not be null");

        if (direction == null)
            throw new IllegalArgumentException("No direction given");

        if (stones == null || stones.isEmpty())
            throw new IllegalStateException("The word to layout must not be empty");

        this.startingField = startingField;
        this.layoutDirection = direction;
        this.stones = stones;
    }

    public String asWord() {
        return stones.stream()
                .map(stone -> stone.getLetter())
                .map(letter -> letter.toString())
                .collect(Collectors.joining(""));
    }
}

