package net.fischboeck.scrabbleator.model;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @author M.Fischböck
 */
@Data
@AllArgsConstructor
public class Stone {

    private char letter;

    private int value;

    public boolean isWildcard() {
        return letter == '?';
    }
}
