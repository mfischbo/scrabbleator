package net.fischboeck.scrabbleator.model;

import lombok.Getter;
import lombok.Setter;
import net.fischboeck.scrabbleator.states.IGameState;

import java.util.List;

/**
 * @author M.Fischböck
 */
@Getter
public class GameContext {

    /**
     * The games current players
     */
    private Players players;

    /**
     * Points to the current player of the game
     */
    @Setter
    private Player currentPlayer;

    /**
     * After the game is finished, contains a list of winners
     */
    @Setter
    private List<Player> winners;

    /**
     * The current board of the game
     */
    private ScrabbleBoard board;

    /**
     * The bag of stones of the current game
     */
    private StoneBag bag;

    @Setter
    private IGameState gameState;

    public GameContext() {
        this.board = new ScrabbleBoard();
        this.bag = new StoneBag();
    }
}
