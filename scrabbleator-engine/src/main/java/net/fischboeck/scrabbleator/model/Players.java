package net.fischboeck.scrabbleator.model;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * A ring buffer holding a list of players
 *
 * @author M.Fischböck
 */
public class Players {


    private Player[] players;

    /**
     * Creates a new set of players
     * @param players The players to be added to the players list
     */
    public Players(final Player... players) {
        this.players = players;
    }


    /**
     * Returns the next player "sitting" clockwise to the provided one.
     * If there are no players -> returns null
     * If the provided player is the only one -> returns null
     * If the provided player is null -> returns null
     * If the provided player is not available -> returns null.
     *
     * @param player The player to get it's next neighbor
     * @return The players neighbor sitting in clockwise direction.
     */
    public Player getNextClockwise(final Player player) {
        if (this.players.length == 0 || players.length == 1)
            return null;

        if (player == null)
            return null;

        for (int i=0; i < players.length; i++) {
            Player p = players[i];

            if (p.equals(player) && i == players.length-1) {
                return players[0];
            }

            if (p.equals(player) && i < players.length -1) {
                return players[i+1];
            }
        }

        return null; // unknown player
    }

    /**
     * Returns the players neighbor sitting next to him in counter clockwise position.
     * If there are no players -> returns null
     * If the provided player is the only one -> returns null
     * If the provided player is null -> returns null
     * If the provided player is not available -> returns null.
     *
     * @param player The player
     * @return The players neighbor.
     */
    public Player getNextCounterClockwise(final Player player) {

        if (players.length == 0 || players.length == 1)
            return null;

        if (player == null)
            return null;

        for (int i=0; i < players.length; i++) {
            Player p = players[i];

            if (p.equals(player) && i == 0)
                return players[players.length-1];

            if (p.equals(player) && i > 0)
                return players[i-1];
        }
        return null; // unknown player
    }


    /**
     * Removes a player from the list.
     * Ensures that all players remain in the same order after the operation.
     *
     * @param player
     */
    public void remove(Player player) {
        if (players.length == 0)
            return;

        if (players.length == 1) {
            this.players = new Player[0];
            return;
        }

        List<Player> pl = Arrays.stream(this.players)
                .filter(p -> !p.equals(player))
                .collect(Collectors.toList());
        this.players = pl.toArray(new Player[pl.size()-1]);
    }

    /**
     * Returns the size of the players collection
     * @return The number of players
     */
    public int size() {
        return players.length;
    }


    /**
     * Returns the player at the specified position.
     * If the index is less than zero or greater than {@link #size()} -1 returns null.
     * @param index
     * @return
     */
    public Player at(int index) {
        if (index > this.players.length || index < 0)
            return null;

        return this.players[index];
    }

    /**
     * Returns a list of players sorted by score.
     * The player with the highest score is at index 0.
     *
     * @return List of players sorted by score
     */
    public List<Player> getPlayersByScore() {

        List<Player> scoreList = Arrays.asList(this.players);
        scoreList.sort(Comparator.comparingInt(p -> p.getScore()));
        Collections.reverse(scoreList);
        return scoreList;
    }
}
