package net.fischboeck.scrabbleator.model;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author M.Fischböck
 */
public class StoneBag {

    private List<Stone> stones;
    private SecureRandom rnd;

    public StoneBag() {
        this.stones = new ArrayList<>(102);
        this.rnd = new SecureRandom();

        // E
        for (int i=0; i < 15; i++)
            this.stones.add(new Stone('E', 1));

        // N
        for (int i=0; i < 9; i++)
            this.stones.add(new Stone('N', 1));

        // S
        for (int i=0; i < 7; i++)
            this.stones.add(new Stone('S', 1));

        // I, R, T, U
        for (int i=0; i < 6; i++) {
            this.stones.add(new Stone('I', 1));
            this.stones.add(new Stone('R', 1));
            this.stones.add(new Stone('T', 1));
            this.stones.add(new Stone('U', 1));
        }

        // A
        for (int i=0; i < 5; i++) {
            this.stones.add(new Stone('A', 1));
        }

        // D, H, M
        for (int i=0; i < 4; i++) {
            this.stones.add(new Stone('D', 1));
            this.stones.add(new Stone('H', 2));
            this.stones.add(new Stone('M', 3));
        }

        // G, L, O
        for (int i=0; i < 3; i++) {
            this.stones.add(new Stone('G', 2));
            this.stones.add(new Stone('L', 2));
            this.stones.add(new Stone('O', 2));
        }

        // B, C, F, K and wildcards
        for (int i=0; i < 2; i++) {
            this.stones.add(new Stone('B', 3));
            this.stones.add(new Stone('C', 4));
            this.stones.add(new Stone('F', 4));
            this.stones.add(new Stone('K', 4));
            this.stones.add(new Stone('?', 0));
        }

        // W, Z -> 3 points
        this.stones.add(new Stone('W', 3));
        this.stones.add(new Stone('Z', 3));

        // P -> 4 points
        this.stones.add(new Stone('P', 4));

        // Ä, J, Ü, V -> 6 points
        this.stones.add(new Stone('Ä', 6));
        this.stones.add(new Stone('J', 6));
        this.stones.add(new Stone('Ü', 6));
        this.stones.add(new Stone('V', 6));

        // Ö, X -> 8 points
        this.stones.add(new Stone('Ö', 8));
        this.stones.add(new Stone('X', 8));

        // Q, Y -> 10 points
        this.stones.add(new Stone('Q', 10));
        this.stones.add(new Stone('Y', 10));

    }


    /**
     * Returns the specified amount of stones from the bag.
     * If the bag contains lesser than maxAmount stones the remaining stones are returned and the bag is cleared
     *
     * @param maxAmount
     * @return A list of stones picked randomly from the bag.
     */
    public List<Stone> pickRandomly(int maxAmount) {

        // if the bag contains lesser than max amount stones return all and clear the bag
        if (this.stones.size() <= maxAmount) {
            List<Stone> retval = new ArrayList<>(maxAmount);
            retval.addAll(this.stones);
            this.stones.clear();
            return retval;
        }


        List<Stone> retval = new ArrayList<>(maxAmount);
        for (int i=0; i < maxAmount; i++) {

            int idx = rnd.nextInt(this.stones.size());
            Stone stone = this.stones.remove(idx);
            retval.add(stone);
        }
        return retval;
    }


    public void addAll(List<Stone> stones) {
        this.stones.addAll(stones);
    }

    public int size() {
        return this.stones.size();
    }

    public String asStringList() {
        return this.stones.stream()
                .map(stone -> Character.toString(stone.getLetter()))
                .collect(Collectors.joining());
    }
}
