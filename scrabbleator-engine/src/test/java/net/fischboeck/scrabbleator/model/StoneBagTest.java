package net.fischboeck.scrabbleator.model;

import org.junit.Test;

import static junit.framework.TestCase.assertEquals;

/**
 * @author M.Fischböck
 */
public class StoneBagTest {

    @Test
    public void initializesCorrectly() throws Exception {

        StoneBag bag = new StoneBag();
        assertEquals(102, bag.size());

    }
}
