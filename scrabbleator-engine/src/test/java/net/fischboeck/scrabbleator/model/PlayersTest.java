package net.fischboeck.scrabbleator.model;

import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

/**
 * @author M.Fischböck
 */
public class PlayersTest {

    private static Player[] examples;

    static {
        examples = new Player[3];
        examples[0] = new Player("Alfred");
        examples[1] = new Player("Bernd");
        examples[2] = new Player("Claus");

        examples[0].setScore(30);
        examples[1].setScore(50);
        examples[2].setScore(100);
    }


    @Test
    public void size_reportsCorrectSize() throws Exception {
        Players players = new Players(examples);
        assertEquals(3, players.size());
    }

    @Test
    public void remove_worksWithZeroPlayers() throws Exception {
        Players players = new Players();
        players.remove(examples[0]);
        assertEquals(0, players.size());
    }

    @Test
    public void remove_worksIfPlayerNotThere() throws Exception {
        Players players = new Players(examples);
        players.remove(new Player("Dana"));
        assertEquals(3, players.size());
    }

    @Test
    public void remove_canRemovePlayerFromFirstsRingPosition() throws Exception {
        Players players = new Players(examples);
        players.remove(examples[0]);
        assertEquals(2, players.size());
    }

    @Test
    public void remove_canRemovePlayerFromLastRingPosition() throws Exception {
        Players players = new Players(examples);
        players.remove(examples[2]);
        assertEquals(2, players.size());
    }

    @Test
    public void remove_canRemovePlayerFromMiddleRingPosition() throws Exception {
        Players players = new Players(examples);
        players.remove(examples[1]);
        assertEquals(2, players.size());
    }

    @Test
    public void remove_keepsCorrectOrder() throws Exception {
        Players players = new Players(examples);
        players.remove(examples[1]);
        assertEquals(examples[0], players.at(0));
        assertEquals(examples[2], players.at(1));
    }

    @Test
    public void getNextClockwise_returnsNullWhenNotPlayersAvailable() throws Exception {
        Players players = new Players();
        assertNull(players.getNextClockwise(examples[0]));
        assertNull(players.getNextClockwise(null));
    }

    @Test
    public void getNextClockwise_resturnsNullWhenPlayerNotInRing() throws Exception {
        Players players = new Players(examples);
        assertNull(players.getNextClockwise(new Player("Dana")));
    }

    @Test
    public void getNextClockwise_returnsNullIfPlayerIsTheOnlyPlayer() throws Exception {
        Players players = new Players(Arrays.copyOfRange(examples, 0, 1));
        assertNull(players.getNextCounterClockwise(examples[0]));
    }

    @Test
    public void getNextClockwise_fromFirstRingPosition() throws Exception {
        Players players = new Players(examples);
        assertEquals(examples[1], players.getNextClockwise(examples[0]));
    }

    @Test
    public void getNextClockwise_fromLastRingPosition() throws Exception {
        Players players = new Players(examples);
        assertEquals(examples[0], players.getNextClockwise(examples[2]));
    }

    @Test
    public void getNextCounterClockwise_returnsNullWhenPlayerNotInRing() throws Exception {
        Players players = new Players();
        assertNull(players.getNextCounterClockwise(examples[0]));
        assertNull(players.getNextCounterClockwise(null));
    }

    @Test
    public void getNextCounterClockwise_returnsNullIfOnlyOnePlayer() throws Exception {
        Players players = new Players(Arrays.copyOfRange(examples, 0, 1));
        assertNull(players.getNextCounterClockwise(examples[0]));
    }

    @Test
    public void getNextCounterClockwise_fromFirstRingPosition() throws Exception {
        Players players = new Players(examples);
        assertEquals(examples[2], players.getNextCounterClockwise(examples[0]));
    }

    @Test
    public void getNextCounterClockwise_fromLastRingPosition() throws Exception {
        Players players = new Players(examples);
        assertEquals(examples[1], players.getNextCounterClockwise(examples[2]));
    }

    @Test
    public void getByScore_higestScoreAtIndexZero() throws Exception {
        Players players = new Players(examples);
        assertEquals(examples[2], players.getPlayersByScore().get(0));
    }

}
