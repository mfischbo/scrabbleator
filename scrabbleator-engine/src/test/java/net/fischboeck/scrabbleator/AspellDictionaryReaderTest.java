package net.fischboeck.scrabbleator;

import net.fischboeck.scrabbleator.util.AspellDictionaryReader;
import org.apache.commons.io.FileUtils;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.util.Set;
import java.util.UUID;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

/**
 * @author M.Fischböck
 */
public class AspellDictionaryReaderTest {

    private File dictionaryFile;

    private static String[] words = {"Marketingstrategy", "Othello's", "Othello"};



    @Before
    public void setup() throws Exception {
        dictionaryFile = File.createTempFile("test-reader", "txt");
        dictionaryFile.deleteOnExit();
        FileUtils.writeStringToFile(dictionaryFile, "Marketingstrategy\nOthello's Othello");
    }


    @Test(expected = IllegalArgumentException.class)
    public void failsOnUnreadableInput() throws Exception {
        AspellDictionaryReader.readDictionary(new File("/tmp/" + UUID.randomUUID().toString()));
    }


    @Test
    public void removesWordsWithIllegalCharacters() throws Exception {
        Set<String> words = AspellDictionaryReader.readDictionary(dictionaryFile);
        assertEquals(1, words.size());
        assertFalse(words.contains("OTHELLO'S"));
        assertFalse(words.contains("Othello's"));
    }

    @Test
    public void uppercasesWords() throws Exception {
        Set<String> words = AspellDictionaryReader.readDictionary(dictionaryFile);
        assertEquals(1, words.size());
        assertTrue(words.contains("OTHELLO"));
    }

    @Test
    public void appliesLengthFilter() throws Exception {
        Set<String> words = AspellDictionaryReader.readDictionary(dictionaryFile);
        assertEquals(1, words.size());
        assertFalse(words.contains("MARKETINGSTRATEGY"));
        assertFalse(words.contains("Marketingstrategy"));
    }
}
