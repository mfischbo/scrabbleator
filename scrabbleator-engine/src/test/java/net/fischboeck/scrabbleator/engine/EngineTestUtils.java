package net.fischboeck.scrabbleator.engine;

import net.fischboeck.scrabbleator.model.BoardField;
import net.fischboeck.scrabbleator.model.ScrabbleBoard;
import net.fischboeck.scrabbleator.model.Stone;
import org.apache.commons.lang3.ArrayUtils;
import org.odftoolkit.simple.SpreadsheetDocument;
import org.odftoolkit.simple.table.Cell;
import org.odftoolkit.simple.table.Table;

import java.io.InputStream;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author M.Fischböck
 */
public class EngineTestUtils {


    static List<Stone> toStonesList(String word) {
        char[] chars = word.toUpperCase().toCharArray();
        return Arrays.stream(ArrayUtils.toObject(chars))
                .map(c -> instanceFor(c))
                .collect(Collectors.toList());
    }


    static List<Stone> toStoneList(Character... chars) {
        return Arrays.stream(chars)
                .map(c -> instanceFor(c))
                .collect(Collectors.toList());
    }

    static Stone instanceFor(Character character) {

        Map<Integer, List<Character>> lookup = new HashMap<>();

        lookup.put(1, Arrays.asList('E' ,'N', 'S', 'I', 'R', 'T', 'U', 'A', 'D'));
        lookup.put(2, Arrays.asList('H', 'G', 'L', 'O'));
        lookup.put(3, Arrays.asList('M', 'B', 'W', 'Z'));
        lookup.put(4, Arrays.asList('C', 'F', 'K', 'P'));
        lookup.put(6, Arrays.asList('Ä', 'J', 'Ü', 'V'));
        lookup.put(8, Arrays.asList('Ö', 'X'));
        lookup.put(10, Arrays.asList('Q', 'Y'));
        lookup.put(0, Arrays.asList('?'));

        int value = lookup.entrySet().stream()
                .filter(e -> e.getValue().contains(character))
                .findFirst()
                .orElseThrow(IllegalArgumentException::new)
                .getKey();
        return new Stone(character, value);
    }


    static ScrabbleBoard fromOdsSpreadsheet(InputStream stream) {

        ScrabbleBoard retval = new ScrabbleBoard();

        try {

            SpreadsheetDocument document = SpreadsheetDocument.loadDocument(stream);
            Table table = document.getSheetByIndex(0);
            for (int y=0; y < 15; y++) {
                for (int x=0; x < 15; x++) {

                    Cell cell = table.getCellByPosition(x, y);
                    String value = cell.getStringValue();
                    if (!value.trim().isEmpty()) {

                        Stone s = instanceFor(value.charAt(0));
                        retval.fieldAt(x, y).setStone(s);
                    }
                }
            }

        } catch (Exception ex) {
            System.err.println("Caught exception while reading ods spreadsheet: " + ex.getMessage());
            ex.printStackTrace();
        }
        return retval;
    }

    static void printBoard(ScrabbleBoard board) {

        for (int y=0; y < 15; y++) {

            printSeparator();
            for (int x = 0; x < 15; x++) {
                System.out.print("|");
                BoardField field = board.fieldAt(x, y);

                if (!field.isEmpty())
                    System.out.print(" " + field.getStone().getLetter() + " ");
                else
                    System.out.print("   ");
            }
            System.out.println("|");
        }
        printSeparator();
    }


    private static void printSeparator() {
        for (int i=0; i <= 30; i++) {
            if (i%2 == 0)
                System.out.print("+");

            if (i%2 == 1)
                System.out.print("---");
        }
        System.out.println("");
    }
}
