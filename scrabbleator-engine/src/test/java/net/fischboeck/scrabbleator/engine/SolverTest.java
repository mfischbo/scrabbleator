package net.fischboeck.scrabbleator.engine;

import net.fischboeck.scrabbleator.model.ScrabbleBoard;
import net.fischboeck.scrabbleator.model.StoneBank;
import net.fischboeck.scrabbleator.model.WordLayout;
import net.fischboeck.scrabbleator.util.AspellDictionaryReader;
import org.junit.Test;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author M.Fischböck
 */
public class SolverTest {

    @Test
    public void canSolveProblems() throws Exception {

        ScrabbleBoard board = EngineTestUtils
                .fromOdsSpreadsheet(getClass().getResourceAsStream("/Scrabble-Test.ods"));

        StoneBank bank = new StoneBank();
        bank.add(EngineTestUtils.toStonesList("ERNSTLF"));

        Set<String> dictionary = AspellDictionaryReader
                .readDictionary(getClass().getResourceAsStream("/dictionary.de"));

        Solver solver = new Solver(dictionary);
        List<Solution> solutions = solver.getSolutions(board, bank);


        EngineTestUtils.printBoard(board);
        int i=0;

        for (Solution s: solutions) {
            Set<WordLayout> layouts = s.getLayouts();

            for (WordLayout l : layouts) {
                String word = l.getStones().stream()
                        .map(stone -> Character.toString(stone.getLetter()))
                        .collect(Collectors.joining());

                if (l.getLayoutDirection() == WordLayout.Direction.Horizontal) {
                    System.out.print("H ");
                } else {
                    System.out.print("V ");
                }

                ScrabbleBoard.Position pos = board.getPositionOf(l.getStartingField());
                System.out.print(pos.x + " " + pos.y + "");
                System.out.print(" [" + l.getScore() + "]\t  => ");
                System.out.println(word);

                i++;
            }
        }

        System.out.println("Found " + i + " solutions");
    }
}
