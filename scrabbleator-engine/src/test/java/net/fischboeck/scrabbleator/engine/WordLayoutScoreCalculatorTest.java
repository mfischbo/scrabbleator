package net.fischboeck.scrabbleator.engine;

import net.fischboeck.scrabbleator.model.ScrabbleBoard;
import net.fischboeck.scrabbleator.model.Stone;
import net.fischboeck.scrabbleator.model.StoneBank;
import net.fischboeck.scrabbleator.model.WordLayout;
import net.fischboeck.scrabbleator.model.WordLayout.Direction;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * @author M.Fischböck
 */
public class WordLayoutScoreCalculatorTest {

    // ---------------------------------------------------------------------------------
    // Checks that bank bonus is calculated correctly
    //---------------------------------------------------------------------------------

    @Test
    public void calculateBankBonus_positiveCase() throws Exception {
        ScrabbleBoard board = new ScrabbleBoard();
        List<Stone> cadmium = EngineTestUtils.toStoneList('C', 'A', 'D', 'M', 'I', 'U', 'M');

        WordLayout layout = new WordLayout(board.fieldAt(0, 0), Direction.Horizontal, cadmium);

        StoneBank bank = new StoneBank();
        bank.add(cadmium);

        assertEquals(50, WordLayoutScoreCalculator.calculateAllFromBankBonus(bank, layout));
    }

    @Test
    public void calculateBankBonus_emptyBank() throws Exception {
        ScrabbleBoard board = new ScrabbleBoard();
        List<Stone> cadmium = EngineTestUtils.toStoneList('C', 'A', 'D', 'M', 'I', 'U', 'M');

        WordLayout layout = new WordLayout(board.fieldAt(0, 0), Direction.Horizontal, cadmium);

        StoneBank bank = new StoneBank();
        assertEquals(0, WordLayoutScoreCalculator.calculateAllFromBankBonus(bank, layout));
    }

    @Test
    public void calculateBankBonus_someFromBank() throws Exception {
        ScrabbleBoard board = new ScrabbleBoard();
        List<Stone> cadmium = EngineTestUtils.toStoneList('C', 'A', 'D', 'M', 'I', 'U', 'M');

        WordLayout layout = new WordLayout(board.fieldAt(0, 0), Direction.Horizontal, cadmium);

        StoneBank bank = new StoneBank();
        bank.add(cadmium);
        bank.remove(Collections.singletonList(cadmium.get(0)));
        assertEquals(0, WordLayoutScoreCalculator.calculateAllFromBankBonus(bank, layout));
    }



    // ---------------------------------------------------------------------------------
    // Checks that word score is calculated correctly -> On empty boards
    // ---------------------------------------------------------------------------------


    @Test
    public void calculateWordScore_emptyBoard_noMultipliers() throws Exception {

        ScrabbleBoard board = new ScrabbleBoard();
        List<Stone> word = EngineTestUtils.toStoneList('T', 'E', 'S', 'T'); // 4-letter word required

        WordLayout layout = new WordLayout(board.fieldAt(0, 4),
                Direction.Horizontal,
                word);

        assertEquals(4, WordLayoutScoreCalculator.calculateWordScore(board, layout));
    }

    @Test
    public void caluclateWordScore_emptyBoard_LetterTimesTwoMulti() throws Exception {

        ScrabbleBoard board = new ScrabbleBoard();
        List<Stone> word = EngineTestUtils.toStoneList('T', 'E', 'S', 'T');

        WordLayout layout = new WordLayout(board.fieldAt(6, 2),
                Direction.Horizontal,
                word);

        assertEquals(6, WordLayoutScoreCalculator.calculateWordScore(board, layout));
    }

    @Test
    public void calculateWordScore_emptyBoard_LetterTimesThreeMulti() throws Exception {
        ScrabbleBoard board = new ScrabbleBoard();
        List<Stone> word = EngineTestUtils.toStoneList('T', 'E', 'S', 'T');

        WordLayout layout = new WordLayout(board.fieldAt(5, 1),
                Direction.Horizontal,
                word);

        assertEquals(6, WordLayoutScoreCalculator.calculateWordScore(board, layout));
    }

    // ---------------------------------------------------------------------------------
    // Checks that word score is calculated correctly -> On non empty boards - no multipliers
    // ---------------------------------------------------------------------------------

    @Test
    public void caluculateWordScore_noMultipliers_singleLetterIntersection() throws Exception {

        // build GUT C2 Horizontal
        ScrabbleBoard board = new ScrabbleBoard();
        board.apply(new WordLayout(board.fieldAtChar('C', 2), Direction.Horizontal,
                EngineTestUtils.toStoneList('G', 'U', 'T')));

        // construct 'TAT'
        List<Stone> word = new ArrayList<>();
        word.add(board.fieldAtChar('E', 2).getStone());
        word.addAll(EngineTestUtils.toStoneList('A', 'T'));

        WordLayout layout = new WordLayout(board.fieldAt(4, 2), Direction.Vertical, word);

        assertEquals(3, WordLayoutScoreCalculator.calculateWordScore(board, layout));
    }

    @Test
    public void calcualteWordScore_noMultipliers_multiLetterIntersection() throws Exception {

        ScrabbleBoard board = new ScrabbleBoard();

        List<Stone> stones = EngineTestUtils.toStoneList('G', 'U', 'T');
        board.apply(new WordLayout(board.fieldAtChar('F', 5), Direction.Horizontal,
                stones));

        List<Stone> newWord = new ArrayList<>();
        newWord.addAll(stones);
        newWord.addAll(EngineTestUtils.toStoneList('E', 'R'));

        WordLayout layout = new WordLayout(board.fieldAtChar('F', 5), Direction.Horizontal,
                newWord);

        assertEquals(6, WordLayoutScoreCalculator.calculateWordScore(board, layout));
    }


    // ------------------------------------------------------------------------------------------
    // Checks that word score is calculated correctly -> On non empty boards - letter multipliers
    // ------------------------------------------------------------------------------------------

    @Test
    public void calculateWordScore_singleIntersection_multiplierNotCountedTwice() throws Exception {

        ScrabbleBoard board = new ScrabbleBoard();

        List<Stone> stones = EngineTestUtils.toStonesList("GUT");
        board.apply(new WordLayout(board.fieldAtChar('G', 3), Direction.Horizontal,
                stones));

        List<Stone> newWord = new ArrayList<>();
        newWord.add(board.fieldAtChar('I', 3).getStone());
        newWord.addAll(EngineTestUtils.toStonesList("AT"));

        WordLayout layout = new WordLayout(board.fieldAtChar('I', 3), Direction.Vertical,
                newWord);

        assertEquals(3, WordLayoutScoreCalculator.calculateWordScore(board, layout));
    }

    @Test
    public void calculateWordScore_singleIntersection_letterTimesThree() throws Exception {

        ScrabbleBoard board = new ScrabbleBoard();
        List<Stone> stones = EngineTestUtils.toStonesList("GUT");
        board.apply(new WordLayout(board.fieldAtChar('D', 4), Direction.Horizontal,
                stones));

        List<Stone> newWord = new ArrayList<>();
        newWord.addAll(EngineTestUtils.toStonesList("TA"));
        newWord.add(board.fieldAtChar('F', 4).getStone());

        WordLayout layout = new WordLayout(board.fieldAtChar('F', 2), Direction.Vertical,
                newWord);

        assertEquals(5, WordLayoutScoreCalculator.calculateWordScore(board, layout));
    }


    @Test
    public void calculateWordScore_multiIntersection_wordTimesThree() throws Exception {

        ScrabbleBoard board = new ScrabbleBoard();
        List<Stone> stones = EngineTestUtils.toStonesList("GUT");
        board.apply(new WordLayout(board.fieldAtChar('E', 1), Direction.Horizontal,
                stones));

        List<Stone> newWord = new ArrayList<>();
        newWord.addAll(stones);
        newWord.addAll(EngineTestUtils.toStonesList("ER"));

        WordLayout layout = new WordLayout(board.fieldAtChar('E', 1), Direction.Horizontal,
                newWord);

        // 4 points for 'GUT' already on board
        // 2 points for 'ER' for expansion => wordscore : 6
        // times 3 for word multiplier => 18

        int wordScore = WordLayoutScoreCalculator.calculateWordScore(board, layout);

        assertEquals(6, wordScore);
        assertEquals(18, WordLayoutScoreCalculator.applyWordMultiplicators(board, layout, wordScore));
    }

    @Test
    public void calculateWordScore_singleIntersection_multipleMultipliers() throws Exception {

        ScrabbleBoard board = new ScrabbleBoard();
        List<Stone> stones = EngineTestUtils.toStonesList("NEIN");
        board.apply(new WordLayout(board.fieldAtChar('K', 4), Direction.Vertical, stones));

        StoneBank bank = new StoneBank();

        List<Stone> newWord = EngineTestUtils.toStonesList("XYLOPHO");
        bank.add(newWord);
        newWord.add(board.fieldAtChar('K', 4).getStone());

        WordLayout layout = new WordLayout(board.fieldAtChar('D', 4), Direction.Horizontal, newWord);


        // X=8 + Y=10 + L=2 + O=2 + P=(4*2) + H=2 + O=1 + N=1 : 35
        // word times 2 multiplier on D4 : 66

        int wordScore = WordLayoutScoreCalculator.calculateWordScore(board, layout);
        assertEquals(35, wordScore);
        assertEquals(70, WordLayoutScoreCalculator.applyWordMultiplicators(board, layout, wordScore));

        // ensure the overall sum from the public method is correct
        assertEquals(120, WordLayoutScoreCalculator.calculateScore(board, bank, layout));
    }
}
