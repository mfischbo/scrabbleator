package net.fischboeck.scrabbleator.engine;

import net.fischboeck.scrabbleator.model.ScrabbleBoard;
import net.fischboeck.scrabbleator.model.WordLayout;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * @author M.Fischböck
 */
public class PartitionerTest {



    @Test
    public void fooTest() throws Exception {

        ScrabbleBoard board = EngineTestUtils.fromOdsSpreadsheet(
                this.getClass().getResourceAsStream("/Scrabble-Test.ods"));

        EngineTestUtils.printBoard(board);

        List<Window> horizontal = new ArrayList<>();
        List<Window> vertical = new ArrayList<>();

        for (int i=0; i < 15; i++) {
            Partitioner partitioner = new Partitioner(board, i);
            horizontal.addAll(partitioner.findPartitions(WordLayout.Direction.Horizontal));
            vertical.addAll(partitioner.findPartitions(WordLayout.Direction.Vertical));
        }


        System.out.println("HORIZONTAL:");
        horizontal.stream()
                .forEach(win -> System.out.println(win.getPattern()));


        System.out.println("VERTICAL:");
        vertical.stream().forEach(win -> System.out.println(win.getPattern()));
    }


}
