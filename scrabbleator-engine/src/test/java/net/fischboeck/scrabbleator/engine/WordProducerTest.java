package net.fischboeck.scrabbleator.engine;

import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * @author M.Fischböck
 */
public class WordProducerTest {

    private final static Set<String> dictionary = new HashSet<>();


    static {
        dictionary.add("BALL");
        dictionary.add("VERHANDLUNGSGESCHICK");
        dictionary.add("BALSAM");
    }


    @Test
    public void findWordsMatchingLetters_correctly() throws Exception {

        WordProducer producer = new WordProducer(dictionary);
        Set<String> matches = producer.findWordsMatchingLetters("BALSAML");

        assertEquals(2, matches.size());
        assertTrue(matches.contains("BALL"));
        assertTrue(matches.contains("BALSAM"));
    }


    /*
    @Test
    public void findWordsMatchingPatternAndLetters() throws Exception {

        WordProducer producer = new WordProducer(dictionary);
        Set<String> matches = producer.findWordsMatchingPattern("+AL+", "BALSAML");


        assertEquals(1, matches.size());
        assertTrue(matches.contains("BALL"));
    }

    @Test
    public void performanceTest() throws Exception {

        Set<String> dict = AspellDictionaryReader.readDictionary(getClass().getResourceAsStream("/dictionary.de"));
        WordProducer producer = new WordProducer(dict);

        long time = System.currentTimeMillis();
        Set<String> matches = producer.findWordsMatchingPattern("+ALE++", "BALSAML");
        matches.forEach(System.out::println);

        System.out.println("Took me " + (System.currentTimeMillis() - time) / 1000.d + " sec.");
    }
    */
}
