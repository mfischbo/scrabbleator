package net.fischboeck.scrabbleator.engine;

import net.fischboeck.scrabbleator.model.ScrabbleBoard;
import net.fischboeck.scrabbleator.model.Stone;
import net.fischboeck.scrabbleator.model.WordLayout;
import org.junit.Test;

import java.util.Arrays;

import static net.fischboeck.scrabbleator.model.WordLayout.Direction;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Tests the correctness of the {@link WordLayoutValidator}.
 *
 * @author M.Fischböck
 */
public class WordLayoutValidatorTest {


    // -------------------------------------------------------------------------
    // Checks for board limits
    // -------------------------------------------------------------------------

    @Test
    public void exceedsBoardLimits_negativeCaseHorizontal() throws Exception {

        ScrabbleBoard board = new ScrabbleBoard();
        WordLayout layout = new WordLayout(board.fieldAt(13, 0),
                Direction.Horizontal,
                EngineTestUtils.toStoneList('A', 'B', 'C'));

        assertTrue(WordLayoutValidator.exceedsBoardLimits(board, layout));
    }

    @Test
    public void exceedsBoardLimits_positiveCaseHorizontal() throws Exception {
        ScrabbleBoard board = new ScrabbleBoard();
        WordLayout layout = new WordLayout(board.fieldAt(5, 0),
                Direction.Horizontal,
                EngineTestUtils.toStoneList('A', 'B', 'C'));
        assertFalse(WordLayoutValidator.exceedsBoardLimits(board, layout));
    }

    @Test
    public void exceedsBoardLimits_edgeCaseHorizontal() throws Exception {
        ScrabbleBoard board = new ScrabbleBoard();
        WordLayout layout = new WordLayout(board.fieldAt(13, 0),
                Direction.Horizontal,
                EngineTestUtils.toStoneList('A', 'B'));
        assertFalse(WordLayoutValidator.exceedsBoardLimits(board, layout));
    }

    @Test
    public void exceedsBoardLimits_negativeCaseVertical() throws Exception {

        ScrabbleBoard board = new ScrabbleBoard();
        WordLayout layout = new WordLayout(board.fieldAt(0, 13),
                Direction.Vertical,
                EngineTestUtils.toStoneList('A', 'B', 'C'));
        assertTrue(WordLayoutValidator.exceedsBoardLimits(board, layout));
    }

    @Test
    public void exceedsBoardLimits_positiveCaseVertical() throws Exception {
        ScrabbleBoard board = new ScrabbleBoard();
        WordLayout layout = new WordLayout(board.fieldAt(0, 5),
                Direction.Vertical,
                EngineTestUtils.toStoneList('A', 'B', 'C'));
        assertFalse(WordLayoutValidator.exceedsBoardLimits(board, layout));
    }

    @Test
    public void exceedsBoardLimits_edgeCaseVertical() throws Exception {
        ScrabbleBoard board = new ScrabbleBoard();
        WordLayout layout = new WordLayout(board.fieldAt(0, 13),
                Direction.Vertical,
                EngineTestUtils.toStoneList('A', 'B'));
        assertFalse(WordLayoutValidator.exceedsBoardLimits(board, layout));
    }

    // -------------------------------------------------------------------------
    // Checks for initial word layout
    // -------------------------------------------------------------------------

    @Test
    public void initialLayoutConditionMatched_negativeCaseHorizontal() throws Exception {
        ScrabbleBoard board = new ScrabbleBoard();
        WordLayout layout = new WordLayout(board.fieldAt(0, 0),
                Direction.Horizontal,
                EngineTestUtils.toStoneList('A', 'B', 'C'));
        assertFalse(WordLayoutValidator.initialLayoutConditionMatched(board, layout));
    }

    @Test
    public void initialLayoutConditionMatched_negativeCaseVertical() throws Exception {
        ScrabbleBoard board = new ScrabbleBoard();
        WordLayout layout = new WordLayout(board.fieldAt(0, 0),
                Direction.Vertical,
                EngineTestUtils.toStoneList('A', 'B', 'C'));
        assertFalse(WordLayoutValidator.initialLayoutConditionMatched(board, layout));
    }

    @Test
    public void initialLayoutConditionMatched_positiveCase() throws Exception {
        ScrabbleBoard board = new ScrabbleBoard();
        WordLayout layout = new WordLayout(board.fieldAt(6, 7),
                Direction.Horizontal,
                EngineTestUtils.toStoneList('T', 'E', 'S', 'T'));
        assertTrue(WordLayoutValidator.initialLayoutConditionMatched(board, layout));
    }

    @Test
    public void initialLayoutConditionMatched_appliesToEmptyBoardOnly() throws Exception {
        ScrabbleBoard board = new ScrabbleBoard();
        Stone a = new Stone('A', 0);
        Stone b = new Stone('B', 0);
        Stone c = new Stone('C', 0);

        board.fieldAt(7, 0).setStone(a);
        board.fieldAt(8, 0).setStone(b);
        board.fieldAt(9, 0).setStone(c);

        WordLayout layout = new WordLayout(board.fieldAt(6, 5),
                Direction.Horizontal,
                EngineTestUtils.toStoneList('T', 'E', 'S', 'T'));
        assertTrue(WordLayoutValidator.initialLayoutConditionMatched(board, layout));
    }


    // -------------------------------------------------------------------------
    // Checks for word intersections
    // -------------------------------------------------------------------------

    @Test
    public void intersectionsAreCorrect_onEmptyBoard() throws Exception {

        ScrabbleBoard board = new ScrabbleBoard();
        WordLayout layout = new WordLayout(board.fieldAt(0, 0),
                Direction.Horizontal,
                EngineTestUtils.toStoneList('T', 'E', 'S', 'T'));
        assertTrue(WordLayoutValidator.intersectionsAreCorrect(board, layout));
    }

    @Test
    public void intersectionsAreCorrect_positiveMatchingCase() throws Exception {

        ScrabbleBoard board = new ScrabbleBoard();
        Stone a = new Stone('A', 0);
        Stone b = new Stone('B', 0);
        Stone c = new Stone('C', 0);

        board.fieldAt(7, 0).setStone(a);
        board.fieldAt(8, 0).setStone(b);
        board.fieldAt(9, 0).setStone(c);


        WordLayout layout = new WordLayout(board.fieldAt(7, 0),
                Direction.Horizontal,
                Arrays.asList(a, b, c));
        assertTrue(WordLayoutValidator.intersectionsAreCorrect(board, layout));
    }


    @Test
    public void intersectionsAreCorrect_simpleNegativeCase() throws Exception {
        ScrabbleBoard board = new ScrabbleBoard();
        Stone a = new Stone('A', 0);
        Stone b = new Stone('B', 0);
        Stone c = new Stone('C', 0);

        board.fieldAt(7, 0).setStone(a);
        board.fieldAt(8, 0).setStone(b);
        board.fieldAt(9, 0).setStone(c);

        WordLayout layout = new WordLayout(board.fieldAt(7, 0), Direction.Horizontal,
                Arrays.asList(a, new Stone('X', 0), c));
        assertFalse(WordLayoutValidator.intersectionsAreCorrect(board, layout));
    }


    @Test
    public void intersectionsAreCorrect_withWildcardField() throws Exception {

        ScrabbleBoard board = new ScrabbleBoard();
        Stone a = new Stone('A', 0);
        Stone b = new Stone('?', 0);
        Stone c = new Stone('C', 0);

        board.fieldAt(7, 0).setStone(a);
        board.fieldAt(8, 0).setStone(b);
        board.fieldAt(9, 0).setStone(c);

        WordLayout layout = new WordLayout(board.fieldAt(7, 0), Direction.Horizontal,
                Arrays.asList(a, new Stone('X', 0), c));
        assertTrue(WordLayoutValidator.intersectionsAreCorrect(board, layout));
    }


    @Test
    public void intersectionsAreCorrect_exceedingBoardBoundary_1() throws Exception {

        ScrabbleBoard board = new ScrabbleBoard();
        WordLayout layout = new WordLayout(board.fieldAt(13, 0), Direction.Horizontal,
                EngineTestUtils.toStoneList('A', 'B', 'C', 'D'));
        assertFalse(WordLayoutValidator.intersectionsAreCorrect(board, layout));
    }


    @Test
    public void intersectionsAreCorrect_exeedingBoardBoundary_2() throws Exception {
        ScrabbleBoard board = new ScrabbleBoard();
        Stone a = new Stone('A', 0);
        Stone b = new Stone('B', 0);
        Stone c = new Stone('C', 0);

        board.fieldAt(12, 0).setStone(a);
        board.fieldAt(13, 0).setStone(b);
        board.fieldAt(14, 0).setStone(c);

        WordLayout layout = new WordLayout(board.fieldAt(12, 0), Direction.Horizontal,
                Arrays.asList(a, b, c, new Stone('D', 0)));
        assertFalse(WordLayoutValidator.intersectionsAreCorrect(board, layout));
    }
}
