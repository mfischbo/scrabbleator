'use strict';

var webpack = require('webpack');
var HtmlWebpackPlugin = require('html-webpack-plugin');

/**
 * Parse the environment
 */


var isProd = process.argv.indexOf('-p') > -1;

module.exports = function makeWebpackConfig() {

    console.log('Running production build? ' + isProd);

    var config = {};

    // configure the entry point of the compiler
    config.entry = {
        app: './index.js'
    };


    // configure the output of the compiler
    config.output = {
        path: __dirname + '/dist',
        filename: '[name].js',
        chunkFilename: '[name].js'
    };

    if (isProd) {
        config.output.path = __dirname + '/../resources/static';
    }

    // configure how the debug source maps should look like
    // either: 'source-map or'inline-source-map'
    config.devtool  = 'source-map';


    config.plugins = [];

    // inject the compiled js/css files into index.html template
    config.plugins.push(
        new HtmlWebpackPlugin({
            template: './views/index.html',
            inject: 'body'
        })
    );

    // plugins for prod use
    /*
    if (isProd) {
        config.plugins.push(new webpack.NoEmitOnErrorsPlugin());
        config.plugins.push(new webpack.optimize.UglifyJsPlugin());
    }
    */

    // configure the loaders
    config.module = {
        rules: [{
            test: /\.js$/,
            loader: 'babel-loader',
            exclude: /node_modules/
        }]
    };

    // proxy requests to the backend
    config.devServer = {};
    config.devServer.proxy = {
        '/api': {
            target: 'http://localhost:8080'
        }
    };

    return config;
}();